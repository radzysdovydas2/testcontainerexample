FROM openjdk:17-alpine
ARG JAR_FILE=/build/libs/testcontainerworkshop.jar
COPY $JAR_FILE testcontainerworkshop.jar
EXPOSE 8080
ENTRYPOINT java -jar /testcontainerworkshop.jar