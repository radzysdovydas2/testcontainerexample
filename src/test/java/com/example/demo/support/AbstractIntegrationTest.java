package com.example.demo.support;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.utility.DockerImageName;
import org.testcontainers.utility.MountableFile;

import java.util.stream.Stream;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
public abstract class AbstractIntegrationTest {

    static final GenericContainer<?> redis = new GenericContainer<>("redis:6-alpine").withExposedPorts(6379);

    static final KafkaContainer kafka = new KafkaContainer(
            DockerImageName.parse("confluentinc/cp-kafka:6.2.1")
    );

    static final PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>("postgres:14-alpine")
            .withCopyFileToContainer(
                    MountableFile.forClasspathResource("/schema.sql"),
                    "/docker-entrypoint-initdb.d/"
            );


    @LocalServerPort
    protected int localServerPort;

    protected RequestSpecification requestSpecification;

    @BeforeEach
    public void prepareRequestSpecification() {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
        requestSpecification = new RequestSpecBuilder()
                .setPort(localServerPort)
                .setContentType(MediaType.APPLICATION_JSON_VALUE)
                .build();
    }

    @DynamicPropertySource
    public static void configureProperties(DynamicPropertyRegistry dynamicPropertyRegistry) {
        Stream.of(redis, kafka, postgres).parallel().forEach(GenericContainer::start);
        dynamicPropertyRegistry.add("spring.redis.host", redis::getHost);
        dynamicPropertyRegistry.add("spring.redis.port", redis::getFirstMappedPort);
        dynamicPropertyRegistry.add("spring.kafka.bootstrap-servers", kafka::getBootstrapServers);
        dynamicPropertyRegistry.add("spring.datasource.url", postgres::getJdbcUrl);
        dynamicPropertyRegistry.add("spring.datasource.username", postgres::getUsername);
        dynamicPropertyRegistry.add("spring.datasource.password", postgres::getPassword);
    }
}
