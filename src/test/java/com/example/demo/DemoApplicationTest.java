package com.example.demo;

import com.example.demo.support.AbstractIntegrationTest;
import io.restassured.filter.log.LogDetail;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class DemoApplicationTest extends AbstractIntegrationTest {

    @Test
    void contextLoads() {
    }

    @Test
    void healthCheck() {
        given(requestSpecification)
                .when()
                .get("/actuator/health")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .log().ifValidationFails(LogDetail.ALL);
    }
}
